package com.danIT.HM1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class DateGame {
    String name;

    public DateGame(String name) {
        this.name = name;
    }

    public void gameDates() {
        Random random = new Random();
        int randomNumber = random.nextInt(7);
        int givenNumber = 0;
        int count = 0;
        int arrSize = 10;
        int[] arr = new int[arrSize];

        String[][] yearOfEvent = new String[][]{
                {"1939", "1961", "1981", "1945", "1919", "1897", "1831"},
                {
                        "When did the World War II begin?",
                        "The first human flight into space?",
                        "Personal computer IBM PC?",
                        "The beginning of the nuclear age?",
                        "Treaty of Versailles?",
                        "Discovery of the electron?",
                        "Faraday's law of electromagnetic induction?"
                },
        };

        System.out.println(yearOfEvent[1][randomNumber]);

        do {
            givenNumber = checkingTheCorrectnessOfTheInput();
            if (arr.length == count) {
                arrSize += 10;
                arr = Arrays.copyOf(arr, arrSize);
            }

            arr[count] = givenNumber;
            count++;

            if (givenNumber > Integer.parseInt(yearOfEvent[0][randomNumber])) {
                System.out.println("Your number is too big. Please, try again.");
            }
            if (givenNumber < Integer.parseInt(yearOfEvent[0][randomNumber])) {
                System.out.println("Your number is too small. Please, try again.");
            }
        } while (givenNumber != Integer.parseInt(yearOfEvent[0][randomNumber]));

        System.out.println("congratulation " + name);
        outputSortArr(arr, count);
    }

    public int checkingTheCorrectnessOfTheInput() {
        String givenNumber;
        Scanner in = new Scanner(System.in);
        boolean check = true;

        do {
            System.out.println("Input your number: ");
            givenNumber = in.nextLine();
            if (givenNumber.isEmpty()) {
                System.out.println("Incorrect number!!! ");
                check = true;
            } else {
                check = false;
            }
            for (int i = 0; i < givenNumber.length(); i++) {
                if (i == 0 && givenNumber.charAt(i) == '-') {
                    if (givenNumber.length() == 1) {
                        System.out.println("Incorrect number!!! ");
                        check = true;
                    } else {
                        check = false;
                    }

                }
                if (Character.digit(givenNumber.charAt(i), 10) < 0) {
                    System.out.println("Incorrect number!!! ");
                    check = true;
                } else {
                    check = false;
                }
            }

        } while (check != false);

        return Integer.parseInt(givenNumber);

    }

    public void outputSortArr(int[] unSortArr, int count) {
        System.out.print("Your numbers: ");
        int[] sortArr = bubbleSort(Arrays.copyOf(unSortArr, count));
        System.out.println(Arrays.toString(Arrays.copyOf(sortArr, count)));
    }

    public int[] bubbleSort(int[] intArray) {

        int n = intArray.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {

                if (intArray[j - 1] < intArray[j]) {
                    temp = intArray[j - 1];
                    intArray[j - 1] = intArray[j];
                    intArray[j] = temp;
                }

            }
        }

        return intArray;
    }
}
