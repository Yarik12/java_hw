package com.danIT.HM1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Choose which game to play:\n" +
                "enter 1 - Random numbers game\n" +
                "enter 2 - Random Date game");

        int gameVariations = Integer.parseInt(in.nextLine());
        switch (gameVariations) {

            case 1: {
                System.out.printf("Let the game begin!");
                System.out.print("\nEnter your name: ");
                String name = in.nextLine();
                Game game = new Game(name);
                game.gameNumbers();
            }break;
            case 2: {
                System.out.printf("Let the game begin!");
                System.out.print("\nEnter your name: ");
                String name = in.nextLine();
                DateGame dateGame = new DateGame(name);
                dateGame.gameDates();
            }break;

        }
    }
}
