package com.danIT.HM2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    String[][] seaPlace = new String[][]{
            {"0", "1", "2", "3", "4", "5"},
            {"1", "-", "-", "-", "-", "-"},
            {"2", "-", "-", "-", "-", "-"},
            {"3", "-", "-", "-", "-", "-"},
            {"4", "-", "-", "-", "-", "-"},
            {"5", "-", "-", "-", "-", "-"},
    };

    void shootingRange() {



        System.out.println("All Set. Get ready to rumble!");

        int givenLine = 0;
        int givenColum = 0;
        int[] targetInArea = settingTarget();
        int[] hits = new int[6];
        do {
            System.out.print("Input line: ");
            givenLine = checkNumber();
            System.out.print("Input column: ");
            givenColum = checkNumber();

            if (givenLine == targetInArea[1] & givenColum == targetInArea[0]) {
                seaPlace[givenLine][givenColum] = "x";
                hits[0] = targetInArea[0];
                hits[1] = targetInArea[1];
            } else if (givenLine == targetInArea[3] & givenColum == targetInArea[2]) {
                seaPlace[givenLine][givenColum] = "x";
                hits[2] = targetInArea[2];
                hits[3] = targetInArea[3];
            } else if (givenLine == targetInArea[5] & givenColum == targetInArea[4]) {
                seaPlace[givenLine][givenColum] = "x";
                hits[4] = targetInArea[4];
                hits[5] = targetInArea[5];
            } else {
                seaPlace[givenLine][givenColum] = "*";
            }

            outputArea(seaPlace);

        } while (!Arrays.equals(targetInArea, hits));
        System.out.println("You have won!");
    }

    public void outputArea(String[][] area) {
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                System.out.print("| " + area[i][j] + " |");
            }
            System.out.println();
        }
    }

    public int checkNumber() {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        while (!(number > 0 && number < 6)) {
            System.out.println("Incorrect number!!!");
            System.out.println("Input number: ");
            number = in.nextInt();
        }
        return number;
    }

    public int[] settingTarget() {
        int[] targetСoordinates = new int[6];
        Random random = new Random();
        int columns = 0;
        int lines = 0;

        int targetOrientation = random.nextInt(2);
        if (targetOrientation == 0) {
            columns = random.nextInt(5 - 1) + 2;
            lines = random.nextInt(6 - 1) + 1;
            for (int i = 0, j = columns - 1; i < targetСoordinates.length; i += 2) {
                targetСoordinates[i + 1] = lines;
                targetСoordinates[i] = j++;
            }
        } else {
            lines = random.nextInt(5 - 1) + 2;
            columns = random.nextInt(6 - 1) + 1;
            for (int i = 0, j = lines - 1; i < targetСoordinates.length; i += 2) {
                targetСoordinates[i] = columns;
                targetСoordinates[i + 1] = j++;
            }
        }


        System.out.println(Arrays.toString(targetСoordinates));
        return targetСoordinates;
    }
}
