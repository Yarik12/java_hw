package com.danIT.HW3;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class TaskPlanner {
    private String[][] scedule = new String[7][2];
    Scanner in = new Scanner(System.in);
    public String[][] fillingOutTheSchedule() {
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film;";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "pass the English exam;";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go to the doctor;";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "pay for utilities;";
        scedule[5][0] = "Friday";
        scedule[5][1] = "write a resume;";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "go have a beer with friends;";

        return scedule;
    }

    public void selectTaskOnADay(String[][] task) {

        String day;
        do {
            System.out.println("Please, input the day of the week:");
            day = in.nextLine();
            day = day.toLowerCase(Locale.ROOT).trim();
            switch (day) {
                case "sunday": {
                    System.out.println("Your tasks for Sunday: " + scedule[0][1]);
                }
                break;
                case "monday": {
                    System.out.println("Your tasks for Monday: " + scedule[1][1]);
                }
                break;
                case "tuesday": {
                    System.out.println("Your tasks for Tuesday: " + scedule[2][1]);
                }
                break;
                case "wednesday": {
                    System.out.println("Your tasks for Wednesday: " + scedule[3][1]);
                }
                break;
                case "thursday": {
                    System.out.println("Your tasks for Thursday: " + scedule[4][1]);
                }
                break;
                case "friday": {
                    System.out.println("Your tasks for Friday: " + scedule[5][1]);
                }
                break;
                case "saturday": {
                    System.out.println("Your tasks for Saturday: " + scedule[6][1]);
                }
                break;
                case "exit": {
                    System.out.println("See you later");
                }
                break;
                case "change": {
                    changeScedule();
                }break;
                default: {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
                break;
            }
        } while (!day.equals("exit"));


    }

    public void changeScedule() {
        String dayChange;

        do {
            System.out.println("Please, input the day to change the scedule:");
            dayChange = in.nextLine().toLowerCase().trim();
            if(searchIndex(dayChange) == -1){
                System.out.println("Incorrect day please try again");
            }else {
                System.out.println("Please, input the changes plan changes for: " + scedule[searchIndex(dayChange)][0]);
                scedule[searchIndex(dayChange)][1] = in.nextLine();
                System.out.println("changes have been applied");
            }
        }while (searchIndex(dayChange) == -1);
    }

    public int searchIndex (String day){
        for (int i = 0; i < scedule.length; i++) {
            if(scedule[i][0].toLowerCase(Locale.ROOT).trim().equals(day)){
                return i;
            }
        }
        return -1;
    }

}
